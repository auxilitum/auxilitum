package controllers;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Query;
import com.avaje.ebean.RawSql;
import com.avaje.ebean.RawSqlBuilder;
import models.Offer;
import models.Rating;
import models.User;
import play.data.DynamicForm;
import play.data.Form;
import play.libs.mailer.Email;
import play.libs.mailer.MailerPlugin;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.login;
import views.html.newoffer;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import static play.data.Form.form;
public class Application extends Controller {

    /*
     * Inner class for Login form
     */
    public static class Login {

        public String username;
        public String password;

        /*
         * Method which checks if entered username and password is correct
         */
        public String validate() {
            if (User.authenticate(username, password) == null) {
                return "Invalid Username or Password";
            }
            return null;
        }

    }

    /*
     * Authentication method: Bad request if username/password is false, else renders /start HTML
     */
    public static Result authenticate() {
        Form<Login> loginForm = Form.form(Login.class).bindFromRequest();
        if (loginForm.hasErrors()) {
            return badRequest(login.render(loginForm, "Login"));
        } else {
            session().clear();
            session("username", loginForm.get().username);
            Form<Offer> offer = newOfferForm;
            return ok(views.html.index.render());
        }
    }

    /*
     * Method for logging out user
     */
    public static Result logout() {
        session().clear();
        flash("success", "You've been logged out");
        return redirect("/login"
        );
    }

    /*
     * Renders editProfile.html after clicking edit button
     */
    public static Result editProfile(){
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        String username = session().get("username");
        User user = User.find.where().eq("username", username).findUnique();
        String name = user.getName();
        String lastName = user.getLastName();
        String streetName = user.getStreetName();
        String houseNumber = user.getHouseNumber();
        String postalCode = user.getPostalCode();
        String city = user.getCity();
        String phoneNumber = user.getPhoneNumber();
        String email = user.getEmail();

        return ok(views.html.editProfile.render("Edit Profile", username, name, lastName, streetName, houseNumber, postalCode,
                city, phoneNumber, email));

    }

    /*
     * updates the entered values from editProfile.html
     */
    public static Result updateProfile(){
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        Form<User> form = Form.form(User.class).bindFromRequest();
        User user = User.find.where().eq("username", session().get("username")).findUnique();
        user.username = form.data().get("username") == "" ? "default" : form.data().get("username");
        user.password = form.data().get("password") == "" ? "default" : form.data().get("password");
        //String userType = form.data().get("userType") == "" ? "default" : form.data().get("userType");
        //user.userType = Integer.parseInt(userType);
        user.name = form.data().get("name") == "" ? "default" : form.data().get("name");
        user.lastName = form.data().get("lastName") == "" ? "default" : form.data().get("lastName");
        user.streetName = form.data().get("streetName") == "" ? "default" : form.data().get("streetName");
        user.houseNumber = form.data().get("houseNumber") == "" ? "default" : form.data().get("houseNumber");
        user.postalCode = form.data().get("postalCode") == "" ? "default" : form.data().get("postalCode");
        user.city = form.data().get("city") == "" ? "default" : form.data().get("city");
        user.phoneNumber = form.data().get("phoneNumber") == "" ? "default" : form.data().get("phoneNumber");
        user.email = form.data().get("email") == "" ? "default" : form.data().get("email");
        double rating = user.rating;

        user.update();
        session("username", user.username);

        return ok(views.html.myProfile.render(newRegistrationForm, user.username, user.name, user.lastName,
                user.streetName, user.houseNumber, user.postalCode, user.city, user.phoneNumber, user.email, rating));
    }

    /*
     * Renders editOffer.html after clicking edit button
     */
    public static Result editOffer(){
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        DynamicForm requestData = Form.form().bindFromRequest();
        int id = Integer.parseInt(requestData.get("edit"));
        String title  = Offer.find.where().eq("id", id).findUnique().title;
        String description = Offer.find.where().eq("id", id).findUnique().description;
        int rph = Offer.find.where().eq("id", id).findUnique().rph;
        int duration = Offer.find.where().eq("id", id).findUnique().duration;
        int permanent = Offer.find.where().eq("id", id).findUnique().permanent;

        return ok(views.html.editOffer.render("Edit Offer", title, description, rph, duration, id, permanent));
    }

    /*
     * updates the entered values from editOffer.html
     */
    public static Result updateValues(){
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        DynamicForm requestData = Form.form().bindFromRequest();
        int id = Integer.parseInt(requestData.get("update"));
        Form<Offer> form = Form.form(Offer.class).bindFromRequest();
        Offer myOffer = Offer.find.where().eq("id", id).findUnique();

        if (form.hasErrors()) {
            myOffer.title = form.data().get("title") == "" ? "default" : form.data().get("title");
            myOffer.description = form.data().get("description") == "" ? "default" : form.data().get("description");
            try {
             //OLD   myOffer.date = form.data().get("date") == "" ? new SimpleDateFormat("dd.MM.YYYY").parse("01.01.2010") : new SimpleDateFormat("dd.MM.YYYY").parse(form.data().get("date") + "." + form.data().get("month") + "." + form.data().get("year"));
              //NEW..
                myOffer.setDate(form.data().get("date") == "" ? new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2010") : new SimpleDateFormat("dd.MM.yyyy").parse(form.data().get("date") + "." + form.data().get("month") + "." + form.data().get("year"))
                );
            } catch (ParseException e) {
                return ok(form.toString() + "/n" + "hmm");
            }
            String username = session().get("username");
            User user = User.find.where().eq("username", username).findUnique();
            String rph = form.data().get("payment") == "" ? "default" : form.data().get("payment");
            myOffer.rph = Integer.parseInt(rph);
            String duration = form.data().get("duration") == "" ? "default" : form.data().get("duration");
            myOffer.duration = Integer.parseInt(duration);
            String permanent;
            if (form.data().get("permanent") != null) {
                permanent = form.data().get("permanent");
                myOffer.permanent = Integer.parseInt(permanent);
            }
            myOffer.often = form.data().get("often");
            myOffer.createdBy = user.getId();
            myOffer.update();
        }

        return myOffers();
    }

    /*
     * Deletes created Offer.
     */
    public static Result deleteOffer(){
        if(sessionEmpty() != null){
            return ok(login.render(form(Login.class), "Login"));
        }
        DynamicForm requestData = Form.form().bindFromRequest();
        int id = Integer.parseInt(requestData.get("delete"));
        Offer myOffer = Ebean.find(Offer.class, id);
        myOffer.acceptedBy=null;
        myOffer.createdBy=null;
        Ebean.delete(myOffer);
        return myOffers();
    }
    /*
     * Deletes accepted Offer by setting acceptedBy to -1
     */
    public static Result declineOffer(){
        if(sessionEmpty() != null){
            return ok(login.render(form(Login.class), "Login"));
        }
        DynamicForm requestData = Form.form().bindFromRequest();
        int id = Integer.parseInt(requestData.get("decline"));
        Offer myOffer = Offer.find.where().eq("id", id).findUnique();
        myOffer.acceptedBy=null;
        myOffer.update();
        return myOffers();
    }


    /*
     * Register new User and save in Database
     */
    public static Result registerNewUser() {
        Form<User> form = Form.form(User.class).bindFromRequest();
        String username = form.data().get("username");
        String password = form.data().get("password");
        String passwordConfirm = form.data().get("passwordConfirm");
        if(User.find.where().eq("username", username).findUnique()!=null ) {
            flash("existingUser", "User already exists. Enter new username.");
            if(!passwordConfirm.equals(password)){
                flash("difPasswords", "Passwords are different. Please enter same passwords.");
            }
            return ok(views.html.registration.render(form, "Create new Account"));
        }
        if(!passwordConfirm.equals(password)){
                flash("passwordConfirm", "Passwords are different. Please enter same passwords.");
            return ok(views.html.registration.render(form, "Create new Account"));
        }

        User newUser = new User();
        newUser.username = form.data().get("username") == "" ? "default" : form.data().get("username");
        newUser.password = form.data().get("password") == "" ? "default" : form.data().get("password");
        String userType = form.data().get("userType") == "" ? "default" : form.data().get("userType");
        newUser.userType = Integer.parseInt(userType);
        newUser.name = form.data().get("name") == "" ? "default" : form.data().get("name");
        newUser.lastName = form.data().get("lastName") == "" ? "default" : form.data().get("lastName");
        newUser.streetName = form.data().get("streetName") == "" ? "default" : form.data().get("streetName");
        newUser.houseNumber = form.data().get("houseNumber") == "" ? "default" : form.data().get("houseNumber");
        newUser.postalCode = form.data().get("postalCode") == "" ? "default" : form.data().get("postalCode");
        newUser.city = form.data().get("city") == "" ? "default" : form.data().get("city");
        newUser.phoneNumber = form.data().get("phoneNumber") == "" ? "default" : form.data().get("phoneNumber");
        newUser.email = form.data().get("email") == "" ? "default" : form.data().get("email");


        Ebean.save(newUser);
        flash("success", "You are successfully registered after registration.");
        return redirect("/");
    }


    /*
     *Save in database
     */
    public static Result saveValues() {
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        Form<Offer> form = Form.form(Offer.class).bindFromRequest();
        if (form.hasErrors()) {
            try {
                Offer myOffer = new Offer();
                myOffer.title = form.data().get("title") == "" ? "default" : form.data().get("title");
                myOffer.description = form.data().get("description") == "" ? "default" : form.data().get("description");

                String d = form.data().get("date");
                String m = form.data().get("month");
                String y = form.data().get("year");
                String dats = d+"/"+m+"/"+y;
                Date date = null;
                try {
                    date = new SimpleDateFormat("dd/MM/yyyy").parse(dats);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                    myOffer.setDate(date);
                String username = session().get("username");
                User user = User.find.where().eq("username", username).findUnique();

                String rph = form.data().get("payment") == "" ? "default" : form.data().get("payment");
                myOffer.rph = Integer.parseInt(rph);
                String duration = form.data().get("duration") == "" ? "default" : form.data().get("duration");
                myOffer.duration = Integer.parseInt(duration);

                String permanent;
                if (form.data().get("permanent") != null) {
                    permanent = form.data().get("permanent");
                    myOffer.permanent = Integer.parseInt(permanent);
                }
                myOffer.often = form.data().get("often");
                myOffer.createdBy = user.getId();

                Ebean.save(myOffer);

            } catch (NumberFormatException ex) {
                return ok(form.toString() + "/n" + "hmm");
            }
        }
        return redirect("/start/myOffers");
    }
    

    /*Search*/
    public static Result index() {
        return ok(views.html.index.render());
    }

    //New Offer form
    private static final Form<Offer> newOfferForm = Form.form(Offer.class);

    //New Registration form
    private static final Form<User> newRegistrationForm = Form.form(User.class);

    //MyProfileForm
    private static final Form<User> newProfileForm = Form.form(User.class);

    public static Result addNewOffer() {
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        return ok(newoffer.render(newOfferForm, "Creating an Offer"));
    }
    /*
     * Renders registration.html with newRegistrationForm
     */
    public static Result addNewUser() {
        return ok(views.html.registration.render(newRegistrationForm, "Create new Account"));
    }

    /*
     * Renders login.html with login form from Login.class
     */
    public static Result login() {
        return ok(views.html.login.render(form(Login.class), "Login"));
    }

    /*
     * Checks if a user is logged in. If session is empty, no user is logged in. Null for user is logged in.
     */
    public static String sessionEmpty(){
        if(session().isEmpty()){
        return "You are not logged in!";}
        return null;
    }

    /*
     * Renders myProfile.html with values from logged in user.
     */
    public static Result myProfile() {
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        String username = session().get("username");
        User user = User.find.where().eq("username", username).findUnique();
        String name = user.getName();
        String lastName = user.getLastName();
        String streetName = user.getStreetName();
        String houseNumber = user.getHouseNumber();
        String postalCode = user.getPostalCode();
        String city = user.getCity();
        String phoneNumber = user.getPhoneNumber();
        String email = user.getEmail();
        double rating = user.getRating();

        return ok(views.html.myProfile.render(newProfileForm, username, name, lastName, streetName, houseNumber,
                postalCode, city, phoneNumber, email, rating));}

    /*
     * Renders myOffers.html with createdOffers and acceptedOffers lists.
     */
    public static Result myOffers() {
        if(sessionEmpty() != null){
            return ok(views.html.login.render(form(Login.class), "Login"));
        }
        String username = session().get("username");
        User user = User.find.where().eq("username", username).findUnique();
        String name = user.getName();
        int userID = user.getId();
        List<Offer> createdOffers = Offer.find.where().eq("createdBy", userID).findList();
        List<Offer> acceptedOffers = Offer.find.where().eq("acceptedBy", userID).findList();

        return ok(views.html.myOffers.render(name, createdOffers, acceptedOffers));
    }

    public static void testData() {
        Date date = new Date();
        try {
            date = new SimpleDateFormat("dd.MM.YYYY").parse("01.01.2010");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Offer offer = new Offer("Garten work","lets do some garten work today", date, 15);
        offer.save();
    }

    public static Result renderRate(){
        return ok(views.html.rating.render());
    }

    public static Result rate() {

        Form<Offer> form = Form.form(Offer.class).bindFromRequest();

        Rating myOffer = new Rating();
        myOffer.description = form.data().get("description") == "" ? "default" : form.data().get("description");
        String stars = form.data().get("rating-input-1") == "" ? "default" : form.data().get("rating-input-1");
        myOffer.rate = stars;
        Ebean.save(myOffer);
        return myOffers();
    }

    /*
        Peter V.
        Inner class to parameterize form of index.html (seachOffer())
     */
    public static class Search {
        public String date;
        public String keyword;
        public int rph;
        public String username;
        public Integer permanent;

        public Search(){}

        public Search(String date, String keyword, int rph){
            this.date = date;
            this.keyword = keyword;
            this.rph = rph;
        }

        /**
         * returns permanent in correct format
         * @return 0 or 1
         */
        public Integer getPermanent(){
            return permanent == 0 || permanent == null ? 0 : 1;
        }

        /**
         * return username in correct format
         * null or value
         */
        public String getUsername(){
            return username == null || username == "" ? null : username;
        }
        /**
         * return date in correct format (Util.Date)
         * @return null if input is null or empty. Util.Date if input is correct
         */
        public Date getDate(){
            Date utilDate = null;
            if (date == null || date == "") return null;
            try {
               utilDate =  new SimpleDateFormat("dd/MM/yyyy").parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
                utilDate = new Date(new Date().getTime());
            }
            return utilDate;
        }
        /**
         * return rph in correct format (int)
         * @return integer which is bigger or equal 0
         */
        public int getRph(){
            if (rph >= 0) return rph;
            return 0;
        }

        /**
         * return keywords (array) in correct format (String)
         * @return String array with all keywords or null
         */
        public String[] getKeyword(){
            if(keyword == null || keyword == "") return null;
            String[] str = keyword.split(",");
            for(int i = 0; i < str.length ; i++){
                str[i] = str[i].trim();
            }
            return str;
        }

        /**
         * Works only when everything is not NULL - Just for testing
         * @return
         */
        public String toString(){
            return "Date: " + this.getDate() + "Keywords: " + this.getKeyword().toString() + "Rph: " + this.getRph();
        }
    }

    /* Peter V.
        Simulate join of Offer and User. Because our DB is shitty :)
        Used to pass list of Joins to search view
     */
    public static class OfferJoinUser {
        //OFFERS
        public int o_id;
        public String o_title;
        public String o_description;
        public Date o_date;
        public int o_rph;
        public int o_duration;
        public Integer o_permanent;
        public String o_often;
        public Integer o_createdBy;
        public Integer o_acceptedBy;

        //USERS
        public Integer u_id;
        public String u_username;
        public String u_password;
        // "0" for disabled person and "1" for Student
        public int u_userType;
        public String u_name;
        public String u_lastName;
        public String u_streetName;
        public String u_houseNumber;
        public String u_postalCode;
        public String u_city;
        public String u_email;

        /*
        Get shorttext of description with only 80 characters
         */
        public String getShorttext() {
            return this.o_description.substring(0, o_description.length() < 80 ? o_description.length() : 80) + " ...";
        }

        /*
        Return date in simple string format for html views
         */
        public String getStringDate(){
            SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
            return f.format(this.o_date);
        }

        /*
        Set offer side of join
         */
        public void setOffer(Offer o) {
            this.o_id = o.id;
            this.o_title = o.title;
            this.o_description = o.description;
            this.o_date = o.getDate();
            this.o_rph = o.rph;
            this.o_duration = o.duration;
            this.o_permanent = o.permanent;
            this.o_often = o.often;
            this.o_createdBy = o.createdBy;
            this.o_acceptedBy = o.acceptedBy;
        }

        /*
        set user side of join via id
         */
        public void searchAndSetUser(int id) {
            String sql = "SELECT id, username, password, user_type as userType, name, last_name as lastName, street_name as streetName, house_number as houseNumber, postal_code as postalCode, city, email" +
                    " FROM User WHERE id = " + id;
            RawSql rawsql = RawSqlBuilder.parse(sql).create();
            Query<User> query = Ebean.find(User.class);
            query.setRawSql(rawsql);
            List<User> list = query.findList();
            setUser(list.get(0));
        }

        /*
        set user side of join via user
         */
        public void setUser(User u) {
            this.u_id = u.id;
            this.u_username = u.username;
            this.u_password = u.password;
            // "0" for disabled person and "1" for Student
            this.u_userType = u.userType;
            this.u_name = u.name;
            this.u_lastName = u.lastName;
            this.u_streetName = u.streetName;
            this.u_houseNumber = u.houseNumber;
            this.u_postalCode = u.postalCode;
            this.u_city = u.city;
            this.u_email = u.email;
        }
    }
    
    /**Peter V.
     * handles SQL search. Gets input through .index-form
     * @return
     */
    public static Result searchOffer(){
        Form<Search> searchForm = Form.form(Search.class).bindFromRequest();
        String sql;

        Search search = new Search(
                                    searchForm.data().get("date")
                                    , searchForm.data().get("keyword")
                                    , Integer.parseInt(searchForm.data().get("rph"))
                                    );
        search.username = searchForm.data().get("username");
        search.permanent = Integer.parseInt(searchForm.data().get("permanent") == null ? "0" : searchForm.data().get("permanent"));
        search.date = searchForm.data().get("date");
        /* Try to join ... did not work
        sql = "SELECT o.id as o_id,o.title as o_title,o.description as o_description," +
                "o.date as o_date,o.rph as o_rph,o.duration as o_duration,o.permanent as o_permanent,o.often as o_often, o.createdBy as o_createdBy, o.acceptedBy as o_acceptedBy," +
                "u.id as u_id, u.username as u_username, u.userType as u_userType, u.name as u_name, u.lastName as u_lastName," +
                "u.streetName as u_streetName, u.houseNumber as u_houseNumber, u.postalCode as u_postalCode, u.city as u_city, u.email as u_email"+
                " FROM Offer o JOIN User u on o.createdBy = u.id WHERE u.title like '%" + search.getKeyword()[0] + "%'";
        */

        //title like '%" + search.getKeyword()[0] + "%'";
        sql = "SELECT id, title, description, " +
                "date, rph, duration, permanent, often, created_by as createdBy, accepted_by as acceptedBy" +
                " FROM Offer WHERE accepted_by is null AND rph >= " + search.getRph();

        // Add keywords to search query in conjunctive form ... AND ( .. OR .. OR.. )
        if(search.getKeyword() != null) {
            String[] str = search.getKeyword();
            String keys = " AND (";

            for(int i = 0; i < str.length; i++){
                keys = keys + "title like '%" + str[i] + "%' OR description like '%" + str[i] + "%'";
                if(i+1 < str.length) keys = keys + " OR ";
            }
            keys = keys + ") ";
            sql = sql + keys;
        }

        // Add permanent to search query in conjunctive form ... AND ( .. OR .. OR .. )
        if(search.getPermanent() == 1){
            String perm = " AND permanent = 1 ";
            sql = sql + perm;
        }

        //Add date to search query in conjunctive form ... AND ( .. OR .. OR ... )
        if(search.getDate() != null){
            String dats = " AND date = '" + search.date + "' ";
            sql = sql + dats;
        }

        //Run Sql und get Offers
        RawSql rawsql = RawSqlBuilder.parse(sql).create();
        Query<Offer> query = Ebean.find(Offer.class);
        query.setRawSql(rawsql);
        List<Offer> list = query.findList();
        //return (ok(views.html.test.render(list.get(1).createdBy.toString())));

        //Join Offers with corresponding User
        List<OfferJoinUser> joinList = new ArrayList<OfferJoinUser>();
        for(int i = 0; i < list.size(); i++){
            OfferJoinUser join = new OfferJoinUser();
            join.setOffer(list.get(i));
            join.searchAndSetUser(list.get(i).createdBy);
            joinList.add(join);
        }

        if(search.getUsername() != null) {
            for (Iterator<OfferJoinUser> iter = joinList.listIterator(); iter.hasNext(); ) {
                OfferJoinUser j = iter.next();
                if (!j.u_username.equalsIgnoreCase(search.getUsername())) {
                    iter.remove();
                }
            }
        }

        return (ok(views.html.search.render(joinList)));
    }


    /**Peter V.
     * Gets accepted offer and corresponding user
     * @return renders accept.html
     */
    public static Result acceptOffer(Integer o_id, Integer u_id){
        if(session().isEmpty()) return (ok(views.html.login.render(form(Login.class), "Login")));
        OfferJoinUser join = new OfferJoinUser();
        Offer offer = Ebean.find(Offer.class, o_id);
        offer.acceptedBy = getOwnId();
        Ebean.save(offer);
        join.setOffer(offer);
        join.searchAndSetUser(u_id);
        writeEmailToCreator(join);
        writeEmailToAcceptor(join);
        return (ok(views.html.accept.render(join)));
    }

    /**Peter V.
     * Return own id
     * @return return own ID
     */
    public static Integer getOwnId(){
        User ownUser;
        List<User> ownList =  Ebean.find(User.class).where().eq("username", session().get("username")).findList();
        ownUser = ownList.get(0);
        return ownUser.id;
    }

    public static User getOwnObject(){
        User ownUser;
        List<User> ownList =  Ebean.find(User.class).where().eq("username", session().get("username")).findList();
        ownUser = ownList.get(0);
        return ownUser;
    }

    /**Peter V.
     * Writes an email from auxiliTUM to user who created the Offer
     * @param join OfferJoinUser to get Accepted Offer
     * @return Status if email was correct or not
     */
    public static String writeEmailToCreator(OfferJoinUser join){
        final Email email = new Email();
        email.setFrom("AuxiliTUM <AuxiliTUM@gmail.com>");
        email.addTo("velten TO <"+join.u_email+">");
        email.setSubject(session().get("username") + " accepted your offer '" + join.o_title + "'");
        email.setBodyText("Hello Mr./Ms. " + join.u_lastName + "\n\n Thank you very much for using AuxiliTUM \n\n We are happy to inform you" +
                " that your offer '"+ join.o_title + "' has been accepted. \n Please pay " + (join.o_duration*join.o_rph) + " Euro within the next 3 weeks " +
                "via paypal www.paypal.com. \n\n Your partner when you need help \n AuxiliTUM");

        String id = MailerPlugin.send(email);
        return id;
        }

    /**Peter V.
     * Writes an email from auxiliTUM to user who accepted the Offer
     * @param join OfferJoinUser to get Accepted Offer
     * @return Status if email was correct or not
     */
    public static String writeEmailToAcceptor(OfferJoinUser join){
        final Email email = new Email();
        User user = getOwnObject();
        email.setFrom("AuxiliTUM <AuxiliTUM@gmail.com>");
        email.addTo("velten TO <"+user.getEmail()+">");
        email.setSubject("You accepted the offer '" + join.o_title + "'");
        email.setBodyText("Hello Mr./Ms. " + user.getLastName() + "\n\nThank you very much for using AuxiliTUM" +
                        "\n\nYou accepted the offer "+join.o_title+" Please come back to you account when you finished the job." +
                        " We will then pay you the amount via www.paypal.com. Now we provide you the details for your job:\n\n" +
                        "TITLE:         " + join.o_title + "\n" +
                        "DESCRIPTION:   " + join.o_description  + "\n" +
                        "DATE:          " + join.getStringDate()  + "\n" +
                        "RATE PER HOUR: " + join.o_rph  + "\n" +
                        "DURATION:      " + join.o_duration  + "\n" +
                        "NAME:              " + join.u_name  + "\n" +
                        "LAST NAME      " + join.u_lastName  + "\n" +
                        "STREET:        " + join.u_streetName+ " " + join.u_houseNumber  + "\n" +
                        "POSTAL CODE:   " + join.u_postalCode  + " " + join.u_city + "\n" +
                        "EMAIL:         " + join.u_email + "\n\n" +
                        "Your partner when you want to help \n AuxiliTUM");
        String id = MailerPlugin.send(email);
        return id;
    }

    public static Result knowmore(){
        return ok(views.html.knowmore.render());
    }

    public static Result details(String title) {
        return play.mvc.Results.TODO;
    }
}
