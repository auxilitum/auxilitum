package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User extends Model {
    @Id
    @GeneratedValue
    public Integer id;
    public String username;
    public String password;
    // "0" for disabled person and "1" for Student
    public int userType;
    public String name;
    public String lastName;
    public String streetName;
    public String houseNumber;
    public String postalCode;
    public String city;
    public String phoneNumber;
    public String email;
    public double rating;


    //Registration Constructor
    public User(String username, String password, int userType, String name, String lastName,
                String streetName, String houseNumber, String postalCode, String city, String phoneNumber, String email) {
        this.username = username;
        this.password = password;
        this.userType = userType;
        this.name = name;
        this.lastName = lastName;
        this.streetName = streetName;
        this.houseNumber = houseNumber;
        this.postalCode = postalCode;
        this.city = city;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.rating = 0;
    }
    public User(){
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getUserType() {
        return userType;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getCity() {
        return city;
    }

    public String getPhoneNumber() {return phoneNumber;}

    public String getEmail() {
        return email;
    }

    public double getRating() {
        return rating;
    }

    public static User authenticate(String username, String password) {
        return find.where().eq("username", username)
                .eq("password", password).findUnique();
    }
    public static Finder<Long,User> find = new Finder<Long,User>(
            Long.class, User.class
    );
}