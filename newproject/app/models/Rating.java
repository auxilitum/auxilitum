package models;

import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by PCU on 01.07.2015.
 */

@Entity
public class Rating extends Model {
    @Id
    @GeneratedValue
    public int id;
    public String rate;
    public String description;
    public int idWorker;
    public int idHost;

    public Rating(int idHost, String rate, String description, int idWorker) {
        this.idHost = idHost;
        this.description = description;
        this.rate = rate;
        this.idWorker = idWorker;
    }

    public Rating() {

    }
    public static Model.Finder<Long,Rating> find = new Model.Finder<>(
            Long.class, Rating.class
    );
}
