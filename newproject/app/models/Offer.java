package models;


import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


@Entity
public class Offer extends Model {
    @Id
    @GeneratedValue
    public int id;
    //@Constraints.Required
    public String title;
    //@Constraints.Required
    public String description;
    //@Constraints.Required
    private String date;
    //@Constraints.Required
    public int rph;
    public int duration;
    public int permanent;
    public String often;
    public Integer createdBy;
    public Integer acceptedBy;

    public Offer(){
    }

    public Offer(String title, String description, Date date, int rph){
        this.title = title;
        this.description = description;
        this.setDate(date);
        this.rph = rph;
    }

    public Offer(String title){
        this.title = title;
    }

    public Offer(int id,String title,String description, Date date, int ratePerHour) {
        this.id = id;
        this.createdBy = createdBy;
        this.title = title;
        this.description = description;
        this.setDate(date);
        this.rph = ratePerHour;
        createdBy = id;
        acceptedBy = -99;

    }

    public Offer(int createdBy, String title,String description, Date date, int ratePerHour, int duration) {
        this.createdBy = createdBy;
        this.title = title;
        this.description = description;
        this.setDate(date);
        this.rph = ratePerHour;
        this.duration = duration;
    }

    public Offer(int createdBy, String title,String description, Date date, int ratePerHour, int duration, int permanent, String often) {
        this.createdBy = createdBy;
        this.title = title;
        this.description = description;
        this.setDate(date);
        this.rph = ratePerHour;
        this.duration = duration;
        this.permanent = permanent;
        this.often = often;
    }

    public static Finder<Long,Offer> find = new Finder<Long,Offer>(
            Long.class, Offer.class
    );


    public String toString() {
        return String.format("%s", title);
    }

    /**
     * Get Date as java.util.Date.
     * Return null when Error
     * If you want simple String-format take getStringDate
     * @return
     */
    public Date getDate(){
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        Date d;
        try {
            d = f.parse(this.date);
        } catch (ParseException e) {
            d = null;
        }
        return d;
    }
    /**
     * Get Date as standard String. Direct from Database
     * @return
     */
    public String getStringDate(){
        return this.date;
    }

    /**
     * Save java.util.Date to Database -> converts to String
     * @param date
     */
    public void setDate(Date date){
        DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        this.date = f.format(date);
    }

    /**
     * Save Date. USE FORMAT dd.mm.yyyy
     * @param date
     */
    public void setStringDate(String date){
        this.date = date;
    }
}
